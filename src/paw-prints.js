function PawPrint(){
	this.errorOutput = [];
	this.hasError = 0;
	this.fullData = '';

	this.getPrint = function(){

		this.createCanvas('pp-arialPt');
		this.ArialPtTest('pp-arialPt');
		this.recordData('pp-arialPt');

		this.createCanvas('pp-arialPx');
		this.ArialPxTest('pp-arialPx');
		this.recordData('pp-arialPx');

		this.createCanvas('pp-nonsense');
		this.nonsenseTest('pp-nonsense');
		this.recordData('pp-nonsense');

		this.createCanvas('pp-webfontPt');
		this.webfontPtTest('pp-webfontPt');
		this.recordData('pp-webfontPt');

		this.createCanvas('pp-webfontPx');
		this.webfontPxTest('pp-webfontPx');
		this.recordData('pp-webfontPx');

		var output = this.sha1(this.fullData);
		return output;
	}

	this.recordData = function(id){
		var canvas = document.getElementById(id);
		var data = canvas.toDataURL("image/jpeg");
		cleanData = data.substring(23, data.length);

		this.fullData += cleanData;
	}
	this.setError = function(errorText){
		this.hasError = 1;
		this.errorOutput.push(errorText)
	}

	this.createCanvas = function(canvasId){
		var appendTo = $('body');
		var canvasHTML = '<canvas height="150" width="300" id="' + canvasId + '">';
		appendTo.append(canvasHTML);
	}

	/* Individual Tests */
	this.ArialPtTest = function(id){
		var canvas = document.getElementById(id);
		var context = canvas.getContext("2d");
		context.font = "22pt Arial";
		context.textBaseline = "top";

		var gradient=context.createLinearGradient(0,0,canvas.width,0);
		gradient.addColorStop("0","MintCream");
		gradient.addColorStop("0.5","MediumAquamarine");
		gradient.addColorStop("1.0","DarkSlateGray");
		// Fill with gradient
		context.fillStyle=gradient;

		context.fillText("PawPrints.js", 10, 10);
	}
	this.ArialPxTest = function(id){
		var canvas = document.getElementById(id);
		var context = canvas.getContext("2d");
		context.font = "30px Arial";
		context.textBaseline = "top";

		var gradient=context.createLinearGradient(0,0,canvas.width,0);
		gradient.addColorStop("0","LightSalmon");
		gradient.addColorStop("0.5","OrangeRed");
		gradient.addColorStop("1.0","red");
		// Fill with gradient
		context.fillStyle=gradient;

		context.fillText("PawPrints.js", 10, 10);
	}
	this.nonsenseTest = function(id){
		var canvas = document.getElementById(id);
		var context = canvas.getContext("2d");
		context.font = "Nonsense Test Font";
		context.textBaseline = "top";

		var gradient=context.createLinearGradient(0,0,canvas.width,0);
		gradient.addColorStop("0","PowderBlue");
		gradient.addColorStop("0.5","Blue");
		gradient.addColorStop("1.0","MidnightBlue");
		// Fill with gradient
		context.fillStyle=gradient;

		context.fillText("PawPrints.js", 10, 10);
	}

	this.webfontPtTest = function(id){
		WebFont.load({
			google: {
				families: ['Lobster', 'Pacifico']
			},
			active: function(){
				var canvas = document.getElementById(id);
				var context = canvas.getContext("2d");
				context.font = "22pt Pacifico";
				context.textBaseline = "top";

				context.fillText("PawPrints.js", 10, 10);
			}
		});
	}

	this.webfontPxTest = function(id){
		WebFont.load({
			google: {
				families: ['Lobster', 'Pacifico']
			},
			active: function(){
				var canvas = document.getElementById(id);
				var context = canvas.getContext("2d");
				context.font = "37px Pacifico";
				context.textBaseline = "top";

				context.fillText("PawPrints.js", 10, 10);
			}
		});
	}

	this.webglTest = function(){
		var canvas = document.getElementById(id);
		gl = initWebGL(canvas);      

		if (gl) {
			gl.clearColor(0.0, 0.0, 0.0, 1.0);                      // Set clear color to black, fully opaque
			gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
			gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things
			gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);      // Clear the color as well as the depth buffer.
		}
	}

	this.sha1 = function(str) {

		var rotate_left = function (n, s) {
			var t4 = (n << s) | (n >>> (32 - s));
			return t4;
		};

		var cvt_hex = function (val) {
			var str = '';
			var i;
			var v;

			for (i = 7; i >= 0; i--) {
				v = (val >>> (i * 4)) & 0x0f;
				str += v.toString(16);
			}
			return str;
		};

		var blockstart;
		var i, j;
		var W = new Array(80);
		var H0 = 0x67452301;
		var H1 = 0xEFCDAB89;
		var H2 = 0x98BADCFE;
		var H3 = 0x10325476;
		var H4 = 0xC3D2E1F0;
		var A, B, C, D, E;
		var temp;

		// utf8_encode
		str = unescape(encodeURIComponent(str));
		var str_len = str.length;

		var word_array = [];
		for (i = 0; i < str_len - 3; i += 4) {
			j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
			word_array.push(j);
		}

		switch (str_len % 4) {
			case 0:
			i = 0x080000000;
			break;
			case 1:
			i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
			break;
			case 2:
			i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
			break;
			case 3:
			i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
			8 | 0x80;
			break;
		}

		word_array.push(i);

		while ((word_array.length % 16) != 14) {
			word_array.push(0);
		}

		word_array.push(str_len >>> 29);
		word_array.push((str_len << 3) & 0x0ffffffff);

		for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
			for (i = 0; i < 16; i++) {
				W[i] = word_array[blockstart + i];
			}
			for (i = 16; i <= 79; i++) {
				W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
			}

			A = H0;
			B = H1;
			C = H2;
			D = H3;
			E = H4;

			for (i = 0; i <= 19; i++) {
				temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 20; i <= 39; i++) {
				temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 40; i <= 59; i++) {
				temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 60; i <= 79; i++) {
				temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			H0 = (H0 + A) & 0x0ffffffff;
			H1 = (H1 + B) & 0x0ffffffff;
			H2 = (H2 + C) & 0x0ffffffff;
			H3 = (H3 + D) & 0x0ffffffff;
			H4 = (H4 + E) & 0x0ffffffff;
		}

		temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
		return temp.toLowerCase();
	}
}